import React from "react";

const Film = (props) => {
    return (
        <div className="film">
        <h1>Move title: {props.name} </h1>
        <p>Year of issue: {props.year}</p>
        <img className='poster' src={props.img}/>
    </div>
    )
};

export default Film;